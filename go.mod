module MySQLHelper

go 1.16

require (
	github.com/go-sql-driver/mysql v1.5.0
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad
)
