MySQL Connection :  
MYSQL_HOSTNAME 	| MySQL Hostname  
MYSQL_PASSWORD 	| MySQL Password  
MYSQL_USERNAME 	| MySQL Username  
MYSQL_PORT	| MySQL Port  
MYSQL_DATABASE 	| MySQL Database (can be blank)  
  
If arg useSSH = true : open a SSH tunnel  
SSH_HOST 	| SSH Hostname  
SSH_USER 	| SSH Username  
SSH_PORT 	| SSH Port  
SSH_KEYPATH 	| SSH Key Path  

